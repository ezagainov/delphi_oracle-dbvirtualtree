unit InsensPosExUnit;

interface
uses Windows;

function InsensPosEx(const SubStr, S: String; Offset: Integer = 1): Integer;

implementation


type
  TCharUpCaseTable = array [Char] of Char;

var
  CharUpCaseTable: TCharUpCaseTable;

procedure InitCharUpCaseTable(var Table: TCharUpCaseTable);
var
  n: Cardinal;
begin
  for n := 0 to Length(Table) - 1 do
    Table[Char(n)] := Char(n);
  CharUpperBuff(@Table, Length(Table));
end;

function InsensPosEx(const SubStr, S: String; Offset: Integer = 1): Integer;
var
  n: Integer;
  SubStrLength: Integer;
  SLength: Integer;
label
  Fail;
begin
  SLength := length(s);
  if (SLength > 0) and (Offset > 0) then begin
    SubStrLength := length(SubStr);
    Result := Offset;
    while SubStrLength <= SLength - Result + 1 do begin
      for n := 1 to SubStrLength do
        if CharUpCaseTable[SubStr[n]] <> CharUpCaseTable[s[Result + n - 1]] then
          goto Fail;
      exit;
      Fail:
        Inc(Result);
    end;
  end;
  Result := 0;
end;

//...

initialization
  InitCharUpCaseTable(CharUpCaseTable);

end.
