unit MainFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, ALogger,
  Dialogs, VirtualTrees, VirtualTreesEx, VirtualDBTreeEx, Grids, DBGrids, DB,
  DBAccess, Ora, MemDS, Math, StdCtrls, InsensPosExUnit, AVirtualDBStringTreeEx,
  ExtCtrls;

type
  TTick = class
  private
    ftick: Cardinal;
  public
    procedure BeginTick;
    function EndTick: Cardinal;
  end;

type
  TMainForm = class(TForm)
    OraQuery1:      TOraQuery;
    OraSession1:    TOraSession;
    Edit1:          TEdit;
    Button2:        TButton;
    DataSource1:    TDataSource;
    OraQuery1PATH:  TStringField;
    OraQuery1LEVEL: TIntegerField;
    OraQuery1ID:    TIntegerField;
    OraQuery1NAME:  TStringField;
    OraQuery1PARENT_ID: TIntegerField;
    OraQuery1LEAFCOUNT: TIntegerField;
    OraQuery1LEVELINDEX: TIntegerField;
    Panel1: TPanel;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Edit1Change(Sender: TObject);
    procedure EditValidate(const ASender: TObject; const AIsValid: Boolean);
    procedure SearchProgress(const ASender: TObject;  const ACurrent, ATotal: Integer);
    procedure CheckBox1Click(Sender: TObject);
  private
  public
  end;

var
  MainForm:       TMainForm;
  TTk:          TTick;
  Tv: TAVirtualDBStringTreeEx;

implementation

{$R *.dfm}



procedure TMainForm.CheckBox1Click(Sender: TObject);
begin
  if TCheckBox(Sender).Checked then
    Tv.TreeOptions.AnimationOptions :=  Tv.TreeOptions.AnimationOptions + [toAnimatedToggle, toAdvancedAnimatedToggle]
  else
    Tv.TreeOptions.AnimationOptions :=  Tv.TreeOptions.AnimationOptions - [toAnimatedToggle, toAdvancedAnimatedToggle]
end;

procedure TMainForm.Edit1Change(Sender: TObject);
begin
  Tv.Search.SearchText := TEdit(Sender).Text;
end;

procedure TMainForm.Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F3 then
    Tv.Search.FindNext;
end;


procedure TMainForm.EditValidate(const ASender: TObject; const AIsValid: Boolean);
begin
  Edit1.Color := IfThen(AIsValid, clWindow, $00E8E8FF);
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  with TTick.Create do begin
    BeginTick;
    OraQuery1.Active := true;
    ALogger.Info('QueryOpen take %d ticks', [EndTick]);
    Free;
  end;
  Tv := TAVirtualDBStringTreeEx.Create(self) ;
  with Tv do begin
    Parent := self;
    Align := alClient;
    DataSource := DataSource1;
    Search.OnValidate := EditValidate;
    Search.OnSearchProgress := SearchProgress;
  end;
end;

procedure TMainForm.SearchProgress(const ASender: TObject;
  const ACurrent, ATotal: Integer);
begin
  Label1.Caption := Format('Search: curennt %d from %d', [ACurrent, ATotal]);
end;

procedure TTick.BeginTick;
begin
  ftick := GetTickCount;
end;

function TTick.EndTick: Cardinal;
begin
  Result := GetTickCount - ftick;
end;

{ TExpandTreeView }


initialization
  TTk := TTick.Create;

end.
