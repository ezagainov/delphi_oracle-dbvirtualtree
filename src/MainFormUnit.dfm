object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'TreeExample'
  ClientHeight = 582
  ClientWidth = 398
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 398
    Height = 41
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      AlignWithMargins = True
      Left = 11
      Top = 23
      Width = 383
      Height = 13
      Margins.Left = 10
      Margins.Top = 2
      Margins.Bottom = 4
      Align = alBottom
      Caption = 'no search'
      ExplicitWidth = 47
    end
    object Edit1: TEdit
      Left = 1
      Top = 1
      Width = 345
      Height = 20
      Align = alClient
      Color = clWhite
      TabOrder = 0
      OnChange = Edit1Change
      OnKeyDown = Edit1KeyDown
      ExplicitHeight = 21
    end
    object Button2: TButton
      Left = 346
      Top = 1
      Width = 51
      Height = 20
      Align = alRight
      Caption = 'FindNext'
      TabOrder = 1
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 536
    Width = 398
    Height = 46
    Align = alBottom
    Caption = 'Options'
    TabOrder = 1
    object CheckBox1: TCheckBox
      AlignWithMargins = True
      Left = 5
      Top = 18
      Width = 388
      Height = 17
      Align = alTop
      Caption = 'animate'
      TabOrder = 0
      OnClick = CheckBox1Click
    end
  end
  object OraQuery1: TOraQuery
    LocalUpdate = True
    Session = OraSession1
    SQL.Strings = (
      '/* Formatted on 24.09.2015 17:32:12 (QP5 v5.252.13127.32847) */'
      'SELECT PATH,'
      '       "LEVEL",'
      '       ID,'
      '       NAME,'
      '       Parent_id,'
      '       (SELECT COUNT (ID)'
      '          FROM REGION_L'
      '         WHERE parent_id = id)'
      '          AS LEAFCOUNT,'
      '         (DENSE_RANK ()'
      
        '          OVER (PARTITION BY (parent_id) ORDER BY "LEVEL", paren' +
        't_Id, name))'
      '       - 1'
      '          AS LevelIndex'
      '  FROM (           SELECT SYS_CONNECT_BY_PATH (id, '#39'/'#39') AS PATH,'
      '                          LEVEL - 1 AS "LEVEL",'
      '                          ID,'
      '                          NAME,'
      '                          PARENT_ID'
      '                     FROM (SELECT OBJECT_ID AS ID,'
      '                                  REGION_NAME AS NAME,'
      '                                  PARENT_ID AS PARENT_ID'
      '                             FROM REGION_L) r'
      '               START WITH PARENT_ID IS NULL'
      '               CONNECT BY PRIOR ID = PARENT_ID'
      '        ORDER SIBLINGS BY name'
      '        )'
      '        order by 2, name')
    AutoCommit = False
    Options.CacheLobs = False
    AutoCalcFields = False
    FilterOptions = [foCaseInsensitive]
    Left = 48
    Top = 288
    object OraQuery1PATH: TStringField
      FieldName = 'PATH'
      Size = 4000
    end
    object OraQuery1LEVEL: TIntegerField
      FieldName = 'LEVEL'
    end
    object OraQuery1ID: TIntegerField
      FieldName = 'ID'
    end
    object OraQuery1NAME: TStringField
      FieldName = 'NAME'
      Size = 64
    end
    object OraQuery1PARENT_ID: TIntegerField
      FieldName = 'PARENT_ID'
    end
    object OraQuery1LEAFCOUNT: TIntegerField
      FieldName = 'LEAFCOUNT'
    end
    object OraQuery1LEVELINDEX: TIntegerField
      FieldName = 'LEVELINDEX'
    end
  end
  object OraSession1: TOraSession
    PoolingOptions.ShareSession = False
    Pooling = False
    Username = 'developer_'
    Password = 'developer'
    Server = 'db30'
    Connected = True
    LoginPrompt = False
    Schema = 'argus_sys'
    Left = 80
    Top = 288
  end
  object DataSource1: TDataSource
    DataSet = OraQuery1
    Left = 112
    Top = 288
  end
end
