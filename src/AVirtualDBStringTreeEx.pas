unit AVirtualDBStringTreeEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, VirtualTrees, VirtualTreesEx, VirtualDBTreeEx, Grids, DBGrids, DB,
  DBAccess, Ora, MemDS, Math, StdCtrls, InsensPosExUnit;

  {$M+}

type
  TAEditorValidateNotify = procedure(const ASender: TObject; const AIsValid: Boolean) of object;
  TASearchProgressNotify = procedure(const ASender: TObject; const ACurrent, ATotal: Integer) of object;

  TAVirtualDBStringTreeEx = class;

  TADBTreeIncrementalSearch = class
  private
    FOwner:          TAVirtualDBStringTreeEx;
    FOnValidate:     TAEditorValidateNotify;
    FOnSearchProgress: TASearchProgressNotify;
    FResultPathList: TStringList;
    FSearchText:     String;
    FInSearch:       Boolean;
    FCurrentId, FRecordCount: Integer;
    procedure SetSearchText(const Value: String);
    function DoFindFirst(): Boolean;
    function DoNavigateToSearchResult(const AResultId: Integer): Boolean;
  public
    constructor Create(AOwner: TAVirtualDBStringTreeEx); overload;
    destructor Destroy; override;
    function FindFirst(const ASearchText: String): Boolean;
    function FindNext: Boolean;
  published
    property SearchText: String Read FSearchText Write SetSearchText;
    property CurrentIndex: Integer Read FCurrentId;
    property RecordCount: Integer Read FRecordCount;
    property OnValidate: TAEditorValidateNotify Read FOnValidate Write FOnValidate;
    property OnSearchProgress: TASearchProgressNotify Read FOnSearchProgress Write FOnSearchProgress;
  end;

  TAVirtualDBStringTreeEx = class(TVirtualStringTreeEx)
  private
    type
    PVRec = ^TVrec;

    TVRec = record
      ID, ParentId, Level, LeafCount: Integer;
      Name, Path: String;
    end;
  private
    FLastLevel, FLastIndex: Integer;
    FSearch:     TADBTreeIncrementalSearch;
    FDataSource: TDataSource;
    procedure SetDataSource(ADataSource: TDataSource);
    procedure FOnAfterDatasetOpen(ADataSet: TDataSet);
    procedure FOnBeforeDatasetOpen(ADataSet: TDataSet);
    function NavigateToNodePath(const ANodePath: String): Boolean;
    function FilterLevel(const ADataset: Tdataset; const ALevel: Integer; AParentID: Integer = -1): Integer;
  protected
    procedure DoInitChildren(Node: PVirtualNode; var ChildCount: Cardinal); override;
    procedure DoInitNode(Parent, Node: PVirtualNode; var InitStates: TVirtualNodeInitStates); override;
    procedure DoGetText(Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var Text: UnicodeString); override;
    procedure DoTextDrawing(var PaintInfo: TVTPaintInfo; Text: UnicodeString; CellRect: TRect; DrawFormat: Cardinal); override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property DataSource: TDataSource Read FDataSource Write SetDataSource;
    property Search: TADBTreeIncrementalSearch Read FSearch;
  end;

implementation

{ TAVirtualDBStringTreeEx }

procedure TAVirtualDBStringTreeEx.FOnAfterDatasetOpen(ADataSet: TDataSet);
begin
  Clear;
  RootNodeCount := FilterLevel(ADataSet, 0);
end;

procedure TAVirtualDBStringTreeEx.FOnBeforeDatasetOpen(ADataSet: TDataSet);
begin

end;

constructor TAVirtualDBStringTreeEx.Create(AOwner: TComponent);
begin
  inherited;
  FLastLevel := -1;
  FLastIndex := -1;
  FSearch := TADBTreeIncrementalSearch.Create(Self);
  NodeDataSize := SizeOf(TVRec);
end;

procedure TAVirtualDBStringTreeEx.DoGetText(Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var Text: UnicodeString);
var
  Data: PVRec;
begin
  Data := GetNodeData(Node);
  if Assigned(Data) then
    Text := Data.Name;
  inherited;
end;

procedure TAVirtualDBStringTreeEx.DoInitChildren(Node: PVirtualNode; var ChildCount: Cardinal);
var
  Data: PVRec;
begin
  Data := GetNodeData(Node);
  if Assigned(Data) then
    ChildCount := Data^.LeafCount;
  inherited;
end;

procedure TAVirtualDBStringTreeEx.DoInitNode(Parent, Node: PVirtualNode; var InitStates: TVirtualNodeInitStates);
var
  Data: PVRec;
begin
  Node.CheckType := ctTriStateCheckBox;
  with FDataSource.DataSet do begin
    if Assigned(Parent) then begin
      //�� ������ ������
      Data := GetNodeData(Parent);
    end
    else
      Data := GetNodeData(Node);
    if Assigned(Parent) and (Data.Id > 0) then begin
      //�������� ������ - ������� �� ������ � ID
      if (FLastLevel <> GetNodeLevel(Node)) or (FLastIndex <> Data.Id) then
        FilterLevel(FDataSource.DataSet, GetNodeLevel(Node), Data.Id);
    end
    else
    if (FLastLevel <> GetNodeLevel(Node)) then
      FilterLevel(FDataSource.DataSet, GetNodeLevel(Node));
    Locate('level;LevelIndex', VarArrayOf([GetNodeLevel(Node), Node.Index]), []);

    Data := GetNodeData(Node);
    Data^.Name := FieldByName('name').AsString;
    Data^.ID := FieldByName('id').AsInteger;
    Data^.ParentID := FieldByName('parent_id').AsInteger;
    Data^.LeafCount := FieldByName('LEAFCOUNT').AsInteger;
    Data^.Path := FieldByName('Path').AsString;

    if FieldByName('LEAFCOUNT').AsInteger > 0 then
      InitStates := InitStates + [ivsHasChildren];
  end;
  inherited;
end;

procedure TAVirtualDBStringTreeEx.DoTextDrawing(var PaintInfo: TVTPaintInfo; Text: UnicodeString; CellRect: TRect; DrawFormat: Cardinal);
var
  SubstrPos: Integer;
begin
  inherited;
  if Search.FInSearch then begin
    SubstrPos := 1;
    while SubstrPos <> 0 do begin
      SubstrPos := InsensPosEx(Search.FSearchText, Text, SubstrPos);
      if SubstrPos > 0 then begin
        with PaintInfo.Canvas, CellRect do begin
          Pen.Color := clRed;
          MoveTo(Left + TextWidth(Copy(Text, 1, SubstrPos - 1)), Bottom - 2);
          LineTo(Left + TextWidth(Copy(Text, 1, SubstrPos - 1)) + TextWidth(Copy(Text, SubstrPos, Length(Search.FSearchText))), Bottom - 2);
        end;
        Inc(SubstrPos);
      end;
    end;
  end;
end;

function TAVirtualDBStringTreeEx.FilterLevel(const ADataset: Tdataset; const ALevel: Integer; AParentID: Integer = -1): Integer;
begin
  FLastLevel := ALevel;
  FLastIndex := AParentID;
  ADataset.Filtered := False;
  ADataset.Filter := Format('LEVEL = %d', [ALevel]);
  if AParentID <> -1 then
    ADataset.Filter := ADataset.Filter + Format(' AND parent_id = %d', [AParentID]);

  ADataset.Filtered := True;
  Result := ADataset.RecordCount;
end;

function TAVirtualDBStringTreeEx.NavigateToNodePath(const ANodePath: String): Boolean;
var
  FPathUnserializer: TStringList;
  c: Integer;
  TempNode: PVirtualNode;

  function FindSiblingByData(const AStartNode: PVirtualNode; const AObjectID: Integer): PVirtualNode;
  var
    TmpNode: PVirtualNode;
    Enum: TVTVirtualNodeEnumeration;
  begin
    Result := nil;
    if AStartNode = nil then
      Enum := NoInitNodes()
    else
      Enum := ChildNodes(AStartNode);
    for TmpNode in Enum do begin
      if PVRec(GetNodeData(TmpNode)).id = AObjectID then begin
        Result := TmpNode;
        Exit;
      end;
    end;
  end;

begin
  FPathUnserializer := TStringList.Create;
  TempNode := nil;
  BeginUpdate;
  try
    FPathUnserializer.Delimiter := '/';
    FPathUnserializer.DelimitedText := ANodePath;
    c := 0;
    while c <> FPathUnserializer.Count do begin
      if FPathUnserializer.Strings[c] = EmptyStr then
        FPathUnserializer.Delete(c)
      else begin
        TempNode := FindSiblingByData(TempNode, StrToIntDef(FPathUnserializer.Strings[c], 0));
        Inc(c);
        Assert(Assigned(TempNode), Format('�� ����� ��� %s', [(FPathUnserializer.Strings[c])]));
      end;
    end;
  finally
    FreeAndNil(FPathUnserializer);
    Result := TempNode <> nil;
    if Result then begin
      ClearSelection;
      Selected[TempNode] := True;
      ScrollIntoView(TempNode, True);
    end;
    EndUpdate;
    UpdateScrollBars(True);
  end;
end;

procedure TAVirtualDBStringTreeEx.SetDataSource(ADataSource: TDataSource);
begin
  if FDataSource <> ADataSource then
    FDataSource := ADataSource;
  FDataSource.DataSet.AfterOpen := FOnAfterDatasetOpen;
  FDataSource.DataSet.BeforeOpen := FOnBeforeDatasetOpen;
  if FDataSource.DataSet.Active then
    FOnAfterDatasetOpen(FDataSource.DataSet);
end;

{ TADBTreeIncrementalSearch }

constructor TADBTreeIncrementalSearch.Create(AOwner: TAVirtualDBStringTreeEx);
begin
  FOwner := AOwner;
  FSearchText := EmptyStr;
  FInSearch := False;
  FResultPathList := TStringList.Create;
end;

destructor TADBTreeIncrementalSearch.Destroy;
begin
  FOwner := nil;
  FreeAndNil(FResultPathList);
  inherited;
end;

function TADBTreeIncrementalSearch.DoFindFirst: Boolean;
begin
  //SearchText ��� �������� ����� ��� �������
  Result := False;
  with FOwner.FDataSource.DataSet do begin
    DisableControls;
    try
      FResultPathList.Clear;
      FOwner.FLastLevel := -1;
      FOwner.FLastIndex := -1;

      Filtered := False;
      FilterOptions := [foCaseInsensitive];
      Filter := Format('NAME LIKE ''%%%s%%''', [FSearchText]);
      Filtered := True;

      Self.FCurrentId := -1;
      Self.FRecordCount := RecordCount;
      Result := FRecordCount > 0;

      if Result then begin
        First;
        while not EOF do begin
          FResultPathList.Add(FieldByName('path').AsString);
          Next;
        end;
        Result := DoNavigateToSearchResult(0);
      end
      else begin
        //method shit no found
      end;
    finally
      if Assigned(FOnValidate) then
        FOnValidate(Self, Result);
      EnableControls;
    end;
  end;
end;

function TADBTreeIncrementalSearch.DoNavigateToSearchResult(const AResultId: Integer): Boolean;
begin
  if AResultId < FResultPathList.Count then begin
    FCurrentId := AResultId;
    Result := FOwner.NavigateToNodePath(FResultPathList.Strings[FCurrentId]);
    if Result and Assigned(FOnSearchProgress) then
      FOnSearchProgress(Self, FCurrentId + 1, FRecordCount);
  end
  else
    Result := False;
end;

function TADBTreeIncrementalSearch.FindFirst(const ASearchText: String): Boolean;
begin
  Result := DoFindFirst;
end;

function TADBTreeIncrementalSearch.FindNext: Boolean;
begin
  Result := DoNavigateToSearchResult(FCurrentId + 1);
end;

procedure TADBTreeIncrementalSearch.SetSearchText(const Value: String);
begin
  if FSearchText = Value then
    Exit;
  FSearchText := Value;
  FInSearch := FSearchText <> EmptyStr;
  DoFindFirst;
end;

end.
