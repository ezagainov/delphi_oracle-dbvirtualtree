program ADBTreeExample;

uses
  Forms,
  AVirtualDBStringTreeEx in 'AVirtualDBStringTreeEx.pas',
  InsensPosExUnit in 'InsensPosExUnit.pas',
  MainFormUnit in 'MainFormUnit.pas' {MainForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
